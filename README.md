# python II - 2020

### Tipy
* [Python tips](https://book.pythontips.com/en/latest/index.html)
* [30 seconds of Python](https://github.com/30-seconds/30-seconds-of-python)
* [50+ Python 3 tips](https://medium.com/towards-artificial-intelligence/50-python-3-tips-tricks-e5dbe05212d7)


#### Další tipy
* f strings
```python
things = 10
print(f'Number of things: {things}')
```

* Přiřazení více proměnných
```python
y,x,z = 'a', 'b', 'c'
```

* enumerate
```python
x = [1,2,3]
for i, value in enumerate(x):
    print(i, value)
```

* dir funkce
```python
dir(int)
```

* list comprehension
```python
list_a = [1, 2, 3, 4]
list_b = [2, 3, 4, 5]
common_num = [a for a in list_a for b in list_b if a == b]
print(common_num)
```

* all a any
```python
print(all([True, True, True, 0==0]))
print(any([True, True, True, 0==0]))
```

### Knihy
<img src="https://images-na.ssl-images-amazon.com/images/I/91DGHmkQmjL.jpg" alt="drawing" width="200"/>
[Fluent Python](http://libgen.rs/book/index.php?md5=03766EC4E95D00F769BEF41E5FA320B0)  <br />



<img src="https://images-na.ssl-images-amazon.com/images/I/41uPjEenkFL._SX396_BO1,204,203,200_.jpg" alt="drawing" width="200"/>
[The Pragmatic Programmer: From Journeyman to Master](http://libgen.rs/book/index.php?md5=D01ECC9809ED9B5D9A955913031C7511)  <br />



<img src="https://images-na.ssl-images-amazon.com/images/I/71GgAFsrHxL.jpg" alt="drawing" width="200"/>
[Python Cookbook](http://libgen.rs/book/index.php?md5=874C56CBC659EA2E7BA8B90CFCD1E316)  <br />



<img src="https://m.media-amazon.com/images/I/41HII-0nFEL._SX260_.jpg" alt="drawing" width="200"/>
[The Self-Taught Programmer: The Definitive Guide to Programming Professionally](http://libgen.rs/book/index.php?md5=CD1F458966CF444C046361CDD68FCCE0)  <br />


<img src="https://cdn-blog.adafruit.com/uploads/2018/12/1Capture-550x480.jpg" alt="drawing" width="200"/>
[Art of Computer Programming](http://libgen.rs/search.php?req=Art+of+Computer+Programming&open=0&res=25&view=simple&phrase=1&column=def) <br />


<img src="https://images-na.ssl-images-amazon.com/images/I/51b7XbfMIIL.jpg" alt="drawing" width="200"/> 
[Clean Code: A Handbook of Agile Software Craftsmanship](http://libgen.rs/book/index.php?md5=838CC6AC8CB0D8DDB98FDB1AE0C8A443)  <br />

