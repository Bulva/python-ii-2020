from collections import Counter 


class Card:
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __repr__(self):
        return 'rank: {self.rank} suit: {self.suit}'.format(self=self)


class Player:
    def __init__(self, name, hand):
        self.name = name
        self._hand = hand

    def has_cards(self):
        return bool(len(self._hand))

    def play_card(self, rank=None, suit=None, start=False, seven=False, ace=False):
        if rank is not None:
            for i, c in enumerate(self._hand):
                if c.rank == rank:
                    return self._hand.pop(i)       
            if start is False:
                if seven and rank == '7': 
                    return None 
                if ace and rank == 'A':
                    return None 

        if suit is not None:
            for i, c in enumerate(self._hand):
                if c.suit == suit:
                    return self._hand.pop(i)

        return None

    def dominant_suit(self):
        suits = [c.suit for c in self._hand]
        return Counter(suits).most_common(1)[0][0]

    def add_cards(self, cards):
        self._hand.extend(cards)

    def add_card(self, card):
        self.add_cards([card])

    def no_cards(self):
        return len(self._hand)