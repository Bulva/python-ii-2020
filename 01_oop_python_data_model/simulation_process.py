import multiprocessing
from play import play
import time

ITERATIONS = 500000
PROCESSES = 2

def simulate():
    start_time = time.time()
    for i in range(ITERATIONS):
        play()
    print('TIME: {}'.format(time.time() - start_time))

if __name__ == '__main__':
    for i in range(PROCESSES):
        p = multiprocessing.Process(target=simulate)
        p.start()
