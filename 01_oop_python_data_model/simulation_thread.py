from play import play
import time

import threading

ITERATIONS = 250000
THREADS = 4

def simulate():
    start_time = time.time()
    for i in range(ITERATIONS):
        play()
    print('TIME: {}'.format(time.time() - start_time))

for i in range(THREADS):
    t = threading.Thread(target=simulate)
    t.start()