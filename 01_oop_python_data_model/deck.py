from random import shuffle
from game import Card


class Deck:
    RANKS = [str(n) for n in range(7, 11)] + list('JQKA')
    SUITS = ['spades', 'diamonds', 'clubs', 'hearts']

    def __init__(self, cards=None):
        if cards is None:
            # voláme class method prepare_deck(). Protože se jedná o classmethod tak ji voláme přes název třídy
            self._cards = Deck.prepare_deck()
        else:
            self._cards = cards
        # zamíchání karet. Funkce modifikuje list poslaný do funkce a nic nevrací
        shuffle(self._cards)

    @classmethod
    def prepare_deck(cls):
        return [Card(rank, suit) for rank in Deck.RANKS for suit in Deck.SUITS]
    
    def draw_card(self):
        return self.draw_cards(1)[0]

    def draw_cards(self, n):
        try:
            return [self._cards.pop(0) for i in range(n)]
        except IndexError:
            return [self._cards.pop(0) for i in range(len(self._cards))]
    
    def add_card(self, card):
        self._cards.insert(0, card)

    def top_card(self):
        return self._cards[0] 

    def no_cards(self):
        return len(self._cards)

    def clear_cards(self):
        self._cards = []

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]
    
    def __repr__(self):
        return str(self._cards)




