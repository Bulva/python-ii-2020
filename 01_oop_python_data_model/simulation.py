from play import play
import time

start_time = time.time()
ITERATIONS = 1000000

for i in range(ITERATIONS):
    play()

print('TIME: {}'.format(time.time() - start_time))