from game import Player
from deck import Deck


NO_CARDS = 4
DELAY = 1

def play():
    draw_deck = Deck()
    discard_deck = Deck([])

    players = [
            Player('Bonifác', draw_deck.draw_cards(NO_CARDS)),
            Player('Helmut', draw_deck.draw_cards(NO_CARDS))
        ]

    discard_deck.add_card(draw_deck.draw_card())

    playing = 0
    start = True
    seven = 0
    ace = 0
    last_player = None
    suit = None

    while all([player.has_cards() for player in players]):
        if playing > len(players)-1:
            playing = 0

        player = players[playing]
        top_card = discard_deck.top_card()
        
        #print('Top card is {}'.format(top_card))

        #print('{} is choosing a card...'.format(player.name))

        if suit is not None:
            played_card = players[playing].play_card(rank=None, suit=suit)
            suit = None
        else:
            played_card = players[playing].play_card(
                rank=top_card.rank, suit=top_card.suit, start=start, seven=seven, ace=ace
            )

        if played_card is None:
            if top_card.rank == '7' and seven != 0:
                if not draw_deck.no_cards():
                    draw_deck = Deck(discard_deck._cards)
                    discard_deck.clear_cards()
                    discard_deck.add_card(draw_deck.draw_card())
                    print(draw_deck)
                player.add_cards(draw_deck.draw_cards(seven * 2))
                #print('{} took {} cards'.format(player.name, seven * 2))
                seven = 0
            elif top_card.rank == 'A' and ace != 0:
                #print('{} can\'t play\n'.format(player.name))
                ace = 0
                last_player = players[playing]
                playing += 1
                continue
            else:
                if not draw_deck.no_cards():
                    draw_deck = Deck(discard_deck._cards)
                    discard_deck.clear_cards()
                    discard_deck.add_card(draw_deck.draw_card())
                players[playing].add_card(draw_deck.draw_card())
                #print('{} took a card'.format(player.name))
        elif played_card.rank == '7':
            seven += 1
            discard_deck.add_card(played_card)
            #print('{} played {}'.format(player.name, played_card))
        elif played_card.rank == 'A':
            ace = 1
            discard_deck.add_card(played_card)
            #print('{} played {}'.format(player.name, played_card))
        elif played_card.rank == 'J':
            discard_deck.add_card(played_card)
            if not player.no_cards:
                suit = player.dominant_suit()
            #print('{} played a Jack, chose the suit: {}'.format(player.name, suit))
        else:
            discard_deck.add_card(played_card)
            #print('{} played {}'.format(player.name, played_card))

        #print('{} has {} cards'.format(player.name, player.no_cards()))
        #print(' ')
        
        last_player = players[playing]
        playing += 1
        start = False

        if not draw_deck.no_cards():
            draw_deck = Deck(discard_deck._cards)
            discard_deck.clear_cards()
            discard_deck.add_card(draw_deck.draw_card())


    #print('Game ends')
    #print('{} wins the game'.format(last_player.name)) 
