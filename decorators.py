'''
def level_message2(message):
    print(message)

def level_message(func):
    return level_message2('zprava z level message')

level_message(level_message2)
'''
'''
def parent():
    print('pred')
    def child1():
        print('child 1')
    def child2():
        print('child 2')
    child1()
    child2()
    print('po')
    return child2

func = parent()
func()
'''

'''def decorator(func):
    def wrapper():
        print('pred')
        func()
        print('po')
    return wrapper

@decorator
def middle():
    print('prostredek')

middle()
'''
'''
def decorator(func):
    def wrapper(*args, **kwargs):
        print('pred')
        func(*args, **kwargs)
        print('po')
    return wrapper

@decorator
def middle(message):
    print(message)

middle('prostredek')
'''

def logger(func):
    def wrapper(*args, **kwargs):
        print(f'Function: {func.__name__}')
        buy = func(*args, **kwargs)
        print('Finished')
    return wrapper


@logger
def buy(thing):
    print(f'Buying: {thing}')
    return True


buy('candy')

