from collections import Counter 


# Třída, která reprezentuje kartu. Pro defaultní balíček budeme generovat 32 karet (objektů třídy Card)
class Card:
    def __init__(self, rank, suit):
        """Opět konstruktor. Nyní nám stačí pouze ukládat hodnotu a barvu karty

        Args:
            rank (str): hodnota karty: '7','8'...'J','Q','K','A'
            suit (str): barva karty: 'spades', 'hearts'... 
        """
        self.rank = rank
        self.suit = suit

    def __repr__(self):
        """Pokud nedefinujeme tuto metodu, tak nebudeme dostávat smysluplný výpis pro objekty třídy Card
        Budeme dostávat: <main Card.object at 0x025698321320>
        My chceme: rank: 7 suit: spades

        Returns:
            str: výpis informací o kartě
        """
        return 'rank: {self.rank} suit: {self.suit}'.format(self=self)


# Třída, která zastupuje hráče. Každý hráč bude představovat jeden objekt třídy Player
class Player:
    def __init__(self, name, hand):
        """Konstruktor pro vytvoření objektů třídy Player

        Args:
            name (str): název hráče
            hand (list): list, který představuje karty, které má hráč na ruce.
        """
        self.name = name
        self._hand = hand

    def has_cards(self):
        """Metoda, která zjišťuje jesli má hráč v ruce ještě nějaké karty

        Returns:
            bool: True když má v ruce karty. False, když nemá v ruce karty
        """
        return bool(len(self._hand))

    def play_card(self, rank=None, suit=None, start=False, seven=False, ace=False):
        """Metoda, která rozhoduje, kterou kartu hráč zahraje na základě situace ve hře

        Args:
            rank (str, optional): hodnota poslední zahrané karty
            suit (str, optional): barva poslední zahrané karty
            start (bool, optional): příznak jestli se jedná o začátek hry
            seven (bool, optional): příznak jestli byla poslední zahraná karta sedmička
            ace (bool, optional): příznak jestli byla poslední zahraná karta eso

        Returns:
            [type]: [description]
        """
        if rank is not None:
            # funkce enumerate nám přidáva do cyklu index. 
            # Tím index můžeme rovnou použít do funkce pop()
            for i, c in enumerate(self._hand):
                if c.rank == rank:
                    # odstranění karty z ruky (její zahrání)
                    return self._hand.pop(i)       
            if start is False:
                if seven and rank == '7': 
                    return None 
                if ace and rank == 'A':
                    return None 

        if suit is not None:
            for i, c in enumerate(self._hand):
                if c.suit == suit:
                    return self._hand.pop(i)

        return None

    def dominant_suit(self):
        """Metoda, která zjišťuje jaké barvy má hráč na ruce nejvíc

        Returns:
            str: název barvy, které má hráč na ruce nejvíc
        """
        suits = [c.suit for c in self._hand]
        return Counter(suits).most_common(1)[0][0]

    def add_cards(self, cards):
        """Metoda pro přidání karet do ruky. Obdobně jako u třídy Deck

        Args:
            cards (list): karty pro přidání do ruky
        """
        self._hand.extend(cards)

    def add_card(self, card):
        self.add_cards([card])

    def no_cards(self):
        """Metoda, která zjišťuje kolik karet má hráč v ruce

        Returns:
            int: počet karet v ruce
        """
        return len(self._hand)