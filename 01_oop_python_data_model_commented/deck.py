from random import shuffle
from game import Card


# Třída, která reprezentuje balíček karet
class Deck:
    # Proměnné, které obsahující hodnoty karet a barvy zapsané
    # pomocí list comprehension (https://www.programiz.com/python-programming/list-comprehension) 
    RANKS = [str(n) for n in range(7, 11)] + list('JQKA')
    SUITS = ['spades', 'diamonds', 'clubs', 'hearts']

    def __init__(self, cards=None):
        """Konstruktor třídy Deck, který vytváří balíčky karet. Konstruktor se
        volá vždy jako první, když vytváří objekt třídy Deck.

        Podtržítko u názvu atributu (self._cards) označuje privátní atribut objektu. 
        Je to pouze varování, ale k proměnné se dá normálně přistoupit.
        Pokud jsou v názvu dvě podtžítka (self.__cards), tak proměnnou Python skryje a není lehce přístupná
        https://stackoverflow.com/questions/1641219/does-python-have-private-variables-in-classes

        Args:
            cards (list, optional): buď můžeme definovat karty při vytvoření balíčku nebo
            defaultně necháme balíček vytvořit v konstruktoru
        """
        if cards is None:
            # voláme class method prepare_deck(). Protože se jedná o classmethod tak ji voláme přes název třídy
            self._cards = Deck.prepare_deck()
        else:
            self._cards = cards
        # zamíchání karet. Funkce modifikuje list poslaný do funkce a nic nevrací
        shuffle(self._cards)

    @classmethod
    def prepare_deck(cls):
        """Metoda, která připraví balíček karet. Nakombinuje všechny hodnoty a všechny barvy.
        Proměnné definované uvnitř třídy odkazujeme přes název třídy. Vytváříme zde jednotlivé karty (objekty třídy Card)

        Returns:
            list: balíček 32 karet
        """
        return [Card(rank, suit) for rank in Deck.RANKS for suit in Deck.SUITS]
    
    def draw_card(self):
        """Simuluje dobrání jedné karty z balíčku. Funkce volá pouze funkci draw_cards s parametrem n=1
        Metody, které mají ve vstupním parametru self se vztahují k objektům tříd a umožňují přistupovat k atributům objektů

        Returns:
            list: seznam s jednou dobranout kartou
        """
        return self.draw_cards(1)[0]

    def draw_cards(self, n):
        """Obdobně jako u metody draw_card, ale umožňuje dobírat více karet. Funkce pop() vrátí kartu a rovnou ji odmaže z listu

        Args:
            n (int): počet karet na dobrání

        Returns:
            list: seznam dobraných karet
        """
        return [self._cards.pop(0) for i in range(n)]
    
    def add_card(self, card):
        """Metoda pro přidání jedné karty. Používame pro zahrání karty do odhazovacího balíčku

        Args:
            card (Card): objekt třídy Card, který představuje kartu, kterou vložíme do balíčku
        """
        self._cards.insert(0, card)

    def top_card(self):
        """Pouze vrací horní kartu z balíčku. Hodí se pro zjištění poslední zahrané karty

        Returns:
            Card: objekt třídy Card
        """
        return self._cards[0] 

    def __len__(self):
        """Pokud bychom tuto metodu nevytvořili tak nemůžeme volat funkci len na třídu Deck.
        Toto by nefungovalo:
        >>> deck = Deck()
        >>> len(deck)
        >>> 32

        Metoda je definovaná v datovém modelu Pythonu a používají ji datové typy, které už znáte (list, str)
        
        Returns:
            int: délka listu karet v balíčku
        """
        return len(self._cards)

    def __getitem__(self, position):
        """Pokud bychom tuto metodu nevytvořili tak nemůžeme vybírat karty z balíčku jako z listu.
        Nakonec ji nepoužijeme, takže jen na ukázku
        Toto by nefungovalo:
        >>> deck = Deck()
        >>> deck[0]
        >>> rank: 7 suit: spades
        
        Args:
            position (int): pozice, ze které vybíráme kartu. Obdobně jako u listu
        
        Returns:
            Card: Karta na daném indexu (position) v balíčku
        """
        return self._cards[position]
    
    def __repr__(self):
        """Pokud bychom tuto metodu nevytvořili tak nemůžeme volat funkci print na objekt třídy Deck.
        Toto by nefungovalo:
        >>> deck = Deck()
        >>> print(deck)
        >>> [rank: 7 suit: spades, rank: 7 suit: hearts, ...]
        
        Returns:
            str: list balíčku karet převedených na řetězec
        """
        return str(self._cards)




