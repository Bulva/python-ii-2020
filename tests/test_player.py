import unittest
from game import Player, Card


class PlayerTestCase(unittest.TestCase):
    def setUp(self):
        self.player = Player('Testovací Honza', [
            Card('10', 'clubs'),
            Card('J', 'hearts'),
            Card('8', 'clubs')
        ])
        self.player2 = Player('Testovací Honza', [])
    
    def test_has_cards(self):
        self.assertTrue(self.player.has_cards())
        self.assertFalse(self.player2.has_cards())

    def test_dominant_suit(self):
        self.assertEqual(self.player.dominant_suit(), 'clubs')

    def test_add_card(self):
        self.player.add_card(Card('Q', 'clubs'))
        self.assertIn(Card('Q', 'clubs'), self.player._hand)  

    def test_play_card(self):
        self.assertIsNone(self.player.play_card(rank='7', suit='clubs', start=False, seven=True, ace=False))  
        self.assertEqual(
            self.player.play_card(rank='8', suit='diamonds', start=False, seven=False, ace=False),
            Card('8', 'clubs')    
        ) 
    
